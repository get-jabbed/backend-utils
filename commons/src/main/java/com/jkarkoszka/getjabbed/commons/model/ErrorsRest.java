package com.jkarkoszka.getjabbed.commons.model;

import java.util.List;
import lombok.Builder;
import lombok.Value;

/**
 * Model of the error response.
 */
@Value
@Builder
public class ErrorsRest {

  int status;

  List<Error> errors;

  /**
   * Model of the error.
   */
  @Value
  @Builder
  public static class Error {

    String field;
    String code;
    String message;
  }
}
