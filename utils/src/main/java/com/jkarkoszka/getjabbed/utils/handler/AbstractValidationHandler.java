package com.jkarkoszka.getjabbed.utils.handler;

import org.springframework.http.HttpStatus;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

/**
 * Class to help creating validator.
 *
 * @param <T> any class
 * @param <U> validator class
 */
public abstract class AbstractValidationHandler<T, U extends Validator> {

  private final Class<T> validationClass;
  private final U validator;

  /**
   * Method to handle request request with validation.
   *
   * @param request request
   * @return server response
   */
  public Mono<ServerResponse> handleRequest(final ServerRequest request) {
    return request.bodyToMono(this.validationClass)
        .flatMap(body -> {
          Errors errors = new BeanPropertyBindingResult(
              body,
              this.validationClass.getName());
          this.validator.validate(body, errors);

          if (errors.getAllErrors().isEmpty()) {
            return processBody(body, request);
          } else {
            return onValidationErrors(errors, body, request);
          }
        });
  }

  protected AbstractValidationHandler(Class<T> clazz, U validator) {
    this.validationClass = clazz;
    this.validator = validator;
  }

  protected Mono<ServerResponse> onValidationErrors(Errors errors, T body, ServerRequest request) {
    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errors.getAllErrors().toString());
  }

  protected abstract Mono<ServerResponse> processBody(T body, ServerRequest originalRequest);

}
