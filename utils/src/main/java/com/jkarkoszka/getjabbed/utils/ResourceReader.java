package com.jkarkoszka.getjabbed.utils;

import com.google.common.io.Resources;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Class with method to read file as string (for tests).
 */
public class ResourceReader {

  /**
   * Method to read resource as string encoded in UTF-8.
   *
   * @param resourceFilePath resource file path
   * @return content of the file
   */
  public static String readResourceAsString(String resourceFilePath) {
    try {
      var resource = Resources.getResource(resourceFilePath);
      return Resources.toString(resource, StandardCharsets.UTF_8);
    } catch (IOException e) {
      throw new ResourceReaderException(e);
    }
  }

  /**
   * Method to read resource as byte array.
   *
   * @param resourceFilePath resource file path
   * @return content of the file as byte array
   */
  public static byte[] readResourceAsByteArray(String resourceFilePath) {
    try {
      var resource = Resources.getResource(resourceFilePath);
      return Resources.toByteArray(resource);
    } catch (IOException e) {
      throw new ResourceReaderException(e);
    }
  }
}
