package com.jkarkoszka.getjabbed.utils;

/**
 * Exception on reading the resource.
 */
public class ResourceReaderException extends RuntimeException {

  public ResourceReaderException(Throwable cause) {
    super(String.format("Resource reader thrown exception: %s", cause.getMessage()), cause);
  }
}
