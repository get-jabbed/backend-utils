package com.jkarkoszka.getjabbed.utils;

/**
 * Helper class to replace placeholder in String (for tests).
 */
public class JsonString {

  private final String jsonString;

  public JsonString(String jsonString) {
    this.jsonString = jsonString;
  }

  public JsonString replaceAsInt(String placeholder, int value) {
    return new JsonString(jsonString.replace("\"" + placeholder + "\"", String.valueOf(value)));
  }

  public JsonString replaceAsString(String placeholder, String value) {
    return new JsonString(jsonString.replace(placeholder, value));
  }

  @Override
  public String toString() {
    return jsonString;
  }
}
