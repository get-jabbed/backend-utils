package com.jkarkoszka.getjabbed.utils.handler;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class SampleEntity {

  String name;
}
