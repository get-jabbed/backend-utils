package com.jkarkoszka.getjabbed.utils;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class JsonStringTest {

  @Test
  public void shouldReturnTheSameStringIfNothingWasReplaced() {
    //given
    var jsonString = "{name: \"Jakub\", age: 12}";

    //when
    var resultString = new JsonString(jsonString).toString();

    //then
    assertThat(resultString).isEqualTo(jsonString);
  }

  @Test
  public void shouldReturnReplacedStringWithReplacedTextPlaceholder() {
    //given
    var placeholder = "##NAME##";
    var jsonString = "{name: \"" + placeholder + "\", age: 32}";
    var name = "Jakub";
    var expectedResultString = "{name: \"" + name + "\", age: 32}";

    //when
    var resultString = new JsonString(jsonString)
        .replaceAsString(placeholder, name)
        .toString();

    //then
    assertThat(resultString).isEqualTo(expectedResultString);
  }

  @Test
  public void shouldReturnReplacedStringWithReplacedIntPlaceholder() {
    //given
    var placeholder = "##AGE##";
    var jsonString = "{name: \"Jakub\", age: \"" + placeholder + "\"}";
    var age = 32;
    var expectedResultString = "{name: \"Jakub\", age: " + age + "}";

    //when
    var resultString = new JsonString(jsonString)
        .replaceAsInt(placeholder, age)
        .toString();

    //then
    assertThat(resultString).isEqualTo(expectedResultString);
  }
}