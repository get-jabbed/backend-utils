package com.jkarkoszka.getjabbed.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.google.common.io.Resources;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

class ResourceReaderTest {

  @Test
  public void shouldReadResourceAsString() {
    //given
    var filePath = "test.txt";
    var expectedContent = "This is test content.";

    //when
    var content = ResourceReader.readResourceAsString(filePath);

    //then
    assertThat(content).isEqualTo(expectedContent);
  }

  @Test
  public void shouldReadResourceAsByteArray() {
    //given
    var filePath = "test.txt";
    var expectedContentAsByteArray = "This is test content.".getBytes(StandardCharsets.UTF_8);

    //when
    var content = ResourceReader.readResourceAsByteArray(filePath);

    //then
    assertThat(content).isEqualTo(expectedContentAsByteArray);
  }

  @Test
  public void shouldThrowRuntimeExceptionIfReadAsStringFailed() {
    //given
    var filePath = "test.txt";
    var exceptionMessage = "file is broken";
    var expectedExceptionMessage = "Resource reader thrown exception: file is broken";
    try (MockedStatic<Resources> resources = Mockito.mockStatic(Resources.class)) {
      var resource = Mockito.mock(URL.class);
      resources.when(() -> Resources.getResource(filePath))
          .thenReturn(resource);
      resources.when(() -> Resources.toString(resource, StandardCharsets.UTF_8))
          .thenThrow(new IOException(exceptionMessage));

      //when
      var exception = assertThrows(ResourceReaderException.class, () -> {
        ResourceReader.readResourceAsString(filePath);
      });

      //then
      assertThat(exception.getMessage()).isEqualTo(expectedExceptionMessage);
    }
  }


  @Test
  public void shouldThrowRuntimeExceptionIfReadAsByteArrayFailed() {
    //given
    var filePath = "test.txt";
    var exceptionMessage = "file is broken";
    var expectedExceptionMessage = "Resource reader thrown exception: file is broken";
    try (MockedStatic<Resources> resources = Mockito.mockStatic(Resources.class)) {
      var resource = Mockito.mock(URL.class);
      resources.when(() -> Resources.getResource(filePath))
          .thenReturn(resource);
      resources.when(() -> Resources.toByteArray(resource))
          .thenThrow(new IOException(exceptionMessage));

      //when
      var exception = assertThrows(ResourceReaderException.class, () -> {
        ResourceReader.readResourceAsByteArray(filePath);
      });

      //then
      assertThat(exception.getMessage()).isEqualTo(expectedExceptionMessage);
    }
  }
}