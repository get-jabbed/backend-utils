package com.jkarkoszka.getjabbed.utils.handler;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Validator;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

@ExtendWith(MockitoExtension.class)
class AbstractValidationHandlerTest {

  AbstractValidationHandler<SampleEntity, Validator> handler =
      new AbstractValidationHandler<>(SampleEntity.class, new SampleEntityValidator()) {
        @Override
        protected Mono<ServerResponse> processBody(SampleEntity body,
                                                   ServerRequest originalRequest) {
          return ServerResponse.ok().build();
        }
      };

  @Test
  public void shouldReturnOk() {
    //given
    var sampleEntity = SampleEntity.builder().name("test").build();
    var serverRequest = Mockito.mock(ServerRequest.class);
    when(serverRequest.bodyToMono(SampleEntity.class)).thenReturn(Mono.just(sampleEntity));

    //when
    var serverResponse = handler.handleRequest(serverRequest).block();

    //then
    assertThat(serverResponse.statusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void shouldReturnBadRequestWhenValidationFailed() {
    //given
    var sampleEntity = SampleEntity.builder().name("").build();
    var serverRequest = Mockito.mock(ServerRequest.class);
    when(serverRequest.bodyToMono(SampleEntity.class)).thenReturn(Mono.just(sampleEntity));

    //when//then
    try {
      handler.handleRequest(serverRequest).block();
      fail("expected exception");
    } catch (ResponseStatusException responseStatusException) {
      assertThat(responseStatusException.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
    }
  }
}
