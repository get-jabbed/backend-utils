package com.jkarkoszka.getjabbed.utilstest.autoconfigure;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

/**
 * Autoconfiguration for test utils beans.
 */
@Configuration
@ConditionalOnBean(ObjectMapper.class)
@ConditionalOnClass({HttpClientErrorException.class, RestTemplate.class})
@Slf4j
@RequiredArgsConstructor
public class UtilsTestAutoConfiguration {

  private final ObjectMapper objectMapper;

  @Value("${server.port}")
  private int port;

  @Bean
  public ErrorRestExtractor errorRestExtractor() {
    return new ErrorRestExtractor(objectMapper);
  }

  @Bean
  public RestTemplate testRestTemplate() {
    return new RestTemplateBuilder()
        .rootUri("http://localhost:" + port).build();
  }
}
