package com.jkarkoszka.getjabbed.utilstest.autoconfigure;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jkarkoszka.getjabbed.commons.model.ErrorsRest;
import lombok.AllArgsConstructor;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

/**
 * Class to help extract ErrorRest from exception.
 */
@AllArgsConstructor
public class ErrorRestExtractor {

  private final ObjectMapper objectMapper;

  public ErrorsRest extract(HttpClientErrorException httpClientErrorException)
      throws JsonProcessingException {
    return objectMapper.readValue(httpClientErrorException.getResponseBodyAsString(),
        ErrorsRest.class);
  }

  public ErrorsRest extract(HttpServerErrorException httpServerErrorException)
      throws JsonProcessingException {
    return objectMapper.readValue(httpServerErrorException.getResponseBodyAsString(),
        ErrorsRest.class);
  }
}
